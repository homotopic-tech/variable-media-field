{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

-- | --   Module    : Media.VF
--   License   : MIT
--   Stability : experimental
--
-- Simple Variable Media Type
module Media.VF
  ( VF (..),
    VFC (..),
  )
where

import Data.Text
import GHC.Generics

-- | Variable Media Field, specify `u` to
-- determine where media will be sourced.
--
-- @since 0.1.0.0
data VF u where
  Blank :: VF u
  RawText :: Text -> VF u
  Image :: u -> VF u
  Audio :: u -> VF u
  Video :: u -> VF u

deriving stock instance Eq u => Eq (VF u)

deriving stock instance Functor VF

deriving stock instance Generic (VF u)

deriving stock instance Show u => Show (VF u)

-- | Variable Media Field Collection
--
-- @since 0.1.0.0
newtype VFC u = VFC {unVFC :: [VF u]}
  deriving stock (Eq, Show, Generic, Functor)
