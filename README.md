# variable-media-field

Variable media field is a simple GADT for representing a media element.
It is defined:

```
data VF u where
  Blank :: VF u
  RawText :: Text -> VF u
  Image :: u -> VF u
  Audio :: u -> VF u
  Video :: u -> VF u
```

You can specify `u` as the mechanism for including media elements in your
type. Either by paths, uris or some other method.

Also defined is a collection of media elements.

```
data VFC u = VFC { unVFC :: [VF u] }
```
